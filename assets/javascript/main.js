
$(document).ready(function() { 
	var target

	$('#homeCarousel').css('display', 'block');

	$("#toggleNav").css('display', 'none');
	function collapseMenu() {
	  if (window.innerWidth < 768) {
	    $("nav").addClass("closed");
	    $("nav > *").hide();
	    $("#toggleNav").css('display', 'block');
	  } else {
	    $("nav").removeClass("closed");
	    $("nav > *").css("display", "block");
	    $("#toggleNav").css('display', 'none');
	  }
	 };

	$(window).resize(collapseMenu);	


	$('nav a').not($('.news-carousel a')).on('click', function() {
		$('#homeCarousel').hide();
		if (window.innerWidth < 768) {
		  	$("nav").toggleClass("closed");
		  	$("nav > *").toggle();
		  	$('#toggleNav').toggle();
		    $('main').css('opacity', '1').off();
  		}
	});

	$('#toggleNav').on('click', function() {
    $("nav").toggleClass("closed");
    $("nav > *").toggle();
    $(this).toggle();
    $('main').css('opacity', '0.65')
    $('main').one('click', function () {
	    	$("nav").toggleClass("closed");
	    	$("nav > *").toggle();
	    	$('#toggleNav').toggle();
	    	$('main').css('opacity', '1')
   		});
	});



	//  News Carousel ------------------------------------------------------------------------------------ //

	newsCarousel = Object.create(null);
	newsCarousel.element = '.newsCarousel';
	newsCarousel.slideDown = function() {
		if ( !($('.news-carousel div:last').hasClass('news-block-active')) ) {
			active = $('.news-block-active');
			next = active.next();
			active.toggleClass('news-block-active').addClass('news-block-top');
			next.toggleClass('news-block-active').removeClass('news-block-bottom news-block-top');
		} else {
			$('.news-carousel div').addClass('news-block-bottom').removeClass('news-block-top');
			$('.news-carousel div:last').removeClass('news-block-active');
			$('.news-carousel div:first').addClass('news-block-active').removeClass('news-block-bottom');
		}
	};	

	newsCarousel.slideUp = function() {
		if ( !($('.news-carousel div:first').hasClass('news-block-active')) ) {
			active = $('.news-block-active');
			previous = active.prev();
			active.toggleClass('news-block-active').addClass('news-block-bottom');
			previous.toggleClass('news-block-active').removeClass('news-block-bottom news-block-top');
		} else {
			$('.news-carousel div').addClass('news-block-top').removeClass('news-block-bottom');
			$('.news-carousel div:first').removeClass('news-block-active');
			$('.news-carousel div:last').addClass('news-block-active').removeClass('news-block-top');
		}
	};

	newsCarousel.interval = {}
	newsCarousel.setInterval = function(ms = 0) {
		newsCarousel.interval = setInterval(function() {
			newsCarousel.slideDown();
		}, ms);
	};

	newsCarousel.stopInterval = function() {
		clearInterval(newsCarousel.interval);
	}

	newsCarousel.setInterval(5000);

	$('.news-btn-up').on('click', function() {
		newsCarousel.slideUp();
		newsCarousel.stopInterval();
		newsCarousel.setInterval(5000)
	});

	$('.news-btn-down').on('click', function() {
		newsCarousel.slideDown();
		newsCarousel.stopInterval();
		newsCarousel.setInterval(5000)
	});
});


